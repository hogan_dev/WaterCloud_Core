﻿using WaterCloud.DataBase;

namespace WaterCloud.Domain.ContentManage
{
    /// <summary>
    /// 创 建：超级管理员
    /// 日 期：2020-06-09 19:42
    /// 描 述：新闻类别数据映射接口
    /// </summary>
    public interface IArticleCategoryRepository : IRepositoryBase<ArticleCategoryEntity>
    {
    }
}
