﻿using WaterCloud.DataBase;

namespace WaterCloud.Domain.SystemManage
{
    /// <summary>
    /// 创 建：超级管理员
    /// 日 期：2020-07-08 14:33
    /// 描 述：表单设计数据映射接口
    /// </summary>
    public interface IFormRepository : IRepositoryBase<FormEntity>
    {
    }
}
