﻿using WaterCloud.DataBase;

namespace WaterCloud.Domain.SystemOrganize
{
    /// <summary>
    /// 创 建：超级管理员
    /// 日 期：2020-06-01 09:44
    /// 描 述：数据权限数据映射接口
    /// </summary>
    public interface IDataPrivilegeRuleRepository : IRepositoryBase<DataPrivilegeRuleEntity>
    {
    }
}
